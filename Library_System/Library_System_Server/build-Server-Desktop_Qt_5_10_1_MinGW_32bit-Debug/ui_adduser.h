/********************************************************************************
** Form generated from reading UI file 'adduser.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDUSER_H
#define UI_ADDUSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_adduser
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QLineEdit *lineLogin;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QLineEdit *linePassword;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineName;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *lineSurname;
    QPushButton *SaveData;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLineEdit *lineEmail;

    void setupUi(QDialog *adduser)
    {
        if (adduser->objectName().isEmpty())
            adduser->setObjectName(QStringLiteral("adduser"));
        adduser->resize(365, 229);
        gridLayout = new QGridLayout(adduser);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_4 = new QLabel(adduser);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_3->addWidget(label_4);

        lineLogin = new QLineEdit(adduser);
        lineLogin->setObjectName(QStringLiteral("lineLogin"));

        horizontalLayout_3->addWidget(lineLogin);


        gridLayout->addLayout(horizontalLayout_3, 3, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_5 = new QLabel(adduser);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_5->addWidget(label_5);

        linePassword = new QLineEdit(adduser);
        linePassword->setObjectName(QStringLiteral("linePassword"));

        horizontalLayout_5->addWidget(linePassword);


        gridLayout->addLayout(horizontalLayout_5, 5, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(adduser);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        lineName = new QLineEdit(adduser);
        lineName->setObjectName(QStringLiteral("lineName"));

        horizontalLayout->addWidget(lineName);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_2 = new QLabel(adduser);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        lineSurname = new QLineEdit(adduser);
        lineSurname->setObjectName(QStringLiteral("lineSurname"));

        horizontalLayout_2->addWidget(lineSurname);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        SaveData = new QPushButton(adduser);
        SaveData->setObjectName(QStringLiteral("SaveData"));

        gridLayout->addWidget(SaveData, 0, 1, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_3 = new QLabel(adduser);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_4->addWidget(label_3);

        lineEmail = new QLineEdit(adduser);
        lineEmail->setObjectName(QStringLiteral("lineEmail"));

        horizontalLayout_4->addWidget(lineEmail);


        gridLayout->addLayout(horizontalLayout_4, 2, 0, 1, 1);


        retranslateUi(adduser);

        QMetaObject::connectSlotsByName(adduser);
    } // setupUi

    void retranslateUi(QDialog *adduser)
    {
        adduser->setWindowTitle(QApplication::translate("adduser", "Dialog", nullptr));
        label_4->setText(QApplication::translate("adduser", "Login", nullptr));
        label_5->setText(QApplication::translate("adduser", "Password", nullptr));
        label->setText(QApplication::translate("adduser", "Name", nullptr));
        label_2->setText(QApplication::translate("adduser", "Surname", nullptr));
        SaveData->setText(QApplication::translate("adduser", "SaveData", nullptr));
        label_3->setText(QApplication::translate("adduser", "Email", nullptr));
    } // retranslateUi

};

namespace Ui {
    class adduser: public Ui_adduser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDUSER_H
