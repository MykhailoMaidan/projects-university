/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Server/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[15];
    char stringdata0[318];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 18), // "on_Connect_clicked"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 30), // "on_DataBaseUser_button_clicked"
QT_MOC_LITERAL(4, 62, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(5, 84, 34), // "on_ShowDataBaseBook_button_cl..."
QT_MOC_LITERAL(6, 119, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(7, 143, 31), // "on_Authorization_button_clicked"
QT_MOC_LITERAL(8, 175, 30), // "on_Start_Server_button_clicked"
QT_MOC_LITERAL(9, 206, 11), // "StartServer"
QT_MOC_LITERAL(10, 218, 35), // "on_add_new_user_push_button_c..."
QT_MOC_LITERAL(11, 254, 30), // "on_add_book_pushButton_clicked"
QT_MOC_LITERAL(12, 285, 17), // "startClientThread"
QT_MOC_LITERAL(13, 303, 7), // "SOCKET*"
QT_MOC_LITERAL(14, 311, 6) // "socket"

    },
    "MainWindow\0on_Connect_clicked\0\0"
    "on_DataBaseUser_button_clicked\0"
    "on_pushButton_clicked\0"
    "on_ShowDataBaseBook_button_clicked\0"
    "on_pushButton_2_clicked\0"
    "on_Authorization_button_clicked\0"
    "on_Start_Server_button_clicked\0"
    "StartServer\0on_add_new_user_push_button_clicked\0"
    "on_add_book_pushButton_clicked\0"
    "startClientThread\0SOCKET*\0socket"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x08 /* Private */,
       3,    0,   70,    2, 0x08 /* Private */,
       4,    0,   71,    2, 0x08 /* Private */,
       5,    0,   72,    2, 0x08 /* Private */,
       6,    0,   73,    2, 0x08 /* Private */,
       7,    0,   74,    2, 0x08 /* Private */,
       8,    0,   75,    2, 0x08 /* Private */,
       9,    0,   76,    2, 0x08 /* Private */,
      10,    0,   77,    2, 0x08 /* Private */,
      11,    0,   78,    2, 0x08 /* Private */,
      12,    1,   79,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_Connect_clicked(); break;
        case 1: _t->on_DataBaseUser_button_clicked(); break;
        //case 2: _t->on_pushButton_clicked(); break;
        case 3: _t->on_ShowDataBaseBook_button_clicked(); break;
        //case 4: _t->on_pushButton_2_clicked(); break;
        case 5: _t->on_Authorization_button_clicked(); break;
        case 6: _t->on_Start_Server_button_clicked(); break;
        //case 7: _t->StartServer(); break;
        case 8: _t->on_add_new_user_push_button_clicked(); break;
        case 9: _t->on_add_book_pushButton_clicked(); break;
        case 10: _t->startClientThread((*reinterpret_cast< SOCKET*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
