/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_4;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;
    QLineEdit *Login;
    QLabel *label_2;
    QTableView *Table;
    QLabel *label;
    QTextBrowser *Status;
    QLineEdit *Passvord;
    QPushButton *DataBaseUser_button;
    QPushButton *ShowDataBaseBook_button;
    QPushButton *add_book_pushButton;
    QPushButton *add_new_user_push_button;
    QPushButton *Authorization_button;
    QPushButton *Start_Server_button;
    QPushButton *Connect;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(941, 498);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_2->addWidget(label_4);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);

        Login = new QLineEdit(centralWidget);
        Login->setObjectName(QStringLiteral("Login"));

        verticalLayout->addWidget(Login);


        verticalLayout_2->addLayout(verticalLayout);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        Table = new QTableView(centralWidget);
        Table->setObjectName(QStringLiteral("Table"));

        verticalLayout_2->addWidget(Table);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_2->addWidget(label);

        Status = new QTextBrowser(centralWidget);
        Status->setObjectName(QStringLiteral("Status"));

        verticalLayout_2->addWidget(Status);

        Passvord = new QLineEdit(centralWidget);
        Passvord->setObjectName(QStringLiteral("Passvord"));

        verticalLayout_2->addWidget(Passvord);

        DataBaseUser_button = new QPushButton(centralWidget);
        DataBaseUser_button->setObjectName(QStringLiteral("DataBaseUser_button"));

        verticalLayout_2->addWidget(DataBaseUser_button);

        ShowDataBaseBook_button = new QPushButton(centralWidget);
        ShowDataBaseBook_button->setObjectName(QStringLiteral("ShowDataBaseBook_button"));

        verticalLayout_2->addWidget(ShowDataBaseBook_button);

        add_book_pushButton = new QPushButton(centralWidget);
        add_book_pushButton->setObjectName(QStringLiteral("add_book_pushButton"));

        verticalLayout_2->addWidget(add_book_pushButton);

        add_new_user_push_button = new QPushButton(centralWidget);
        add_new_user_push_button->setObjectName(QStringLiteral("add_new_user_push_button"));

        verticalLayout_2->addWidget(add_new_user_push_button);

        Authorization_button = new QPushButton(centralWidget);
        Authorization_button->setObjectName(QStringLiteral("Authorization_button"));

        verticalLayout_2->addWidget(Authorization_button);

        Start_Server_button = new QPushButton(centralWidget);
        Start_Server_button->setObjectName(QStringLiteral("Start_Server_button"));

        verticalLayout_2->addWidget(Start_Server_button);

        Connect = new QPushButton(centralWidget);
        Connect->setObjectName(QStringLiteral("Connect"));

        verticalLayout_2->addWidget(Connect);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 941, 19));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "Passvord", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Login", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Table", nullptr));
        label->setText(QApplication::translate("MainWindow", "Status info", nullptr));
        DataBaseUser_button->setText(QApplication::translate("MainWindow", "Show database user", nullptr));
        ShowDataBaseBook_button->setText(QApplication::translate("MainWindow", "Show database books", nullptr));
        add_book_pushButton->setText(QApplication::translate("MainWindow", "Add new book", nullptr));
        add_new_user_push_button->setText(QApplication::translate("MainWindow", "Add new user", nullptr));
        Authorization_button->setText(QApplication::translate("MainWindow", "Authorization", nullptr));
        Start_Server_button->setText(QApplication::translate("MainWindow", "Start Server", nullptr));
        Connect->setText(QApplication::translate("MainWindow", "Connect", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
