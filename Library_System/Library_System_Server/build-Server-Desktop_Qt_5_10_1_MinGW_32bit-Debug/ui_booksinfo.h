/********************************************************************************
** Form generated from reading UI file 'booksinfo.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOOKSINFO_H
#define UI_BOOKSINFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_BooksInfo
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *bookname_line;
    QPushButton *SaverecordBooks;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QLineEdit *Author_line;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_3;
    QLineEdit *Countris_line;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_4;
    QLineEdit *Genres_line;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_5;
    QLineEdit *Number_of_stock_line;

    void setupUi(QDialog *BooksInfo)
    {
        if (BooksInfo->objectName().isEmpty())
            BooksInfo->setObjectName(QStringLiteral("BooksInfo"));
        BooksInfo->resize(353, 297);
        gridLayout = new QGridLayout(BooksInfo);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(BooksInfo);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        bookname_line = new QLineEdit(BooksInfo);
        bookname_line->setObjectName(QStringLiteral("bookname_line"));

        verticalLayout->addWidget(bookname_line);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        SaverecordBooks = new QPushButton(BooksInfo);
        SaverecordBooks->setObjectName(QStringLiteral("SaverecordBooks"));

        gridLayout->addWidget(SaverecordBooks, 0, 1, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_2 = new QLabel(BooksInfo);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        Author_line = new QLineEdit(BooksInfo);
        Author_line->setObjectName(QStringLiteral("Author_line"));

        verticalLayout_2->addWidget(Author_line);


        gridLayout->addLayout(verticalLayout_2, 1, 0, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_3 = new QLabel(BooksInfo);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_3->addWidget(label_3);

        Countris_line = new QLineEdit(BooksInfo);
        Countris_line->setObjectName(QStringLiteral("Countris_line"));

        verticalLayout_3->addWidget(Countris_line);


        gridLayout->addLayout(verticalLayout_3, 2, 0, 1, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_4 = new QLabel(BooksInfo);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_4->addWidget(label_4);

        Genres_line = new QLineEdit(BooksInfo);
        Genres_line->setObjectName(QStringLiteral("Genres_line"));

        verticalLayout_4->addWidget(Genres_line);


        gridLayout->addLayout(verticalLayout_4, 3, 0, 1, 1);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        label_5 = new QLabel(BooksInfo);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout_5->addWidget(label_5);

        Number_of_stock_line = new QLineEdit(BooksInfo);
        Number_of_stock_line->setObjectName(QStringLiteral("Number_of_stock_line"));

        verticalLayout_5->addWidget(Number_of_stock_line);


        gridLayout->addLayout(verticalLayout_5, 4, 0, 1, 1);


        retranslateUi(BooksInfo);

        QMetaObject::connectSlotsByName(BooksInfo);
    } // setupUi

    void retranslateUi(QDialog *BooksInfo)
    {
        BooksInfo->setWindowTitle(QApplication::translate("BooksInfo", "Dialog", nullptr));
        label->setText(QApplication::translate("BooksInfo", "Book Name", nullptr));
        SaverecordBooks->setText(QApplication::translate("BooksInfo", "Save record", nullptr));
        label_2->setText(QApplication::translate("BooksInfo", "Author", nullptr));
        label_3->setText(QApplication::translate("BooksInfo", "Countris", nullptr));
        label_4->setText(QApplication::translate("BooksInfo", "Genres", nullptr));
        label_5->setText(QApplication::translate("BooksInfo", "Number of stock", nullptr));
    } // retranslateUi

};

namespace Ui {
    class BooksInfo: public Ui_BooksInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOOKSINFO_H
