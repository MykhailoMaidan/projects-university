#ifndef SOCKETSERVER_H
#define SOCKETSERVER_H

#include <winsock2.h>

#include <QMessageBox>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QMessageBox>
#include <QString>
#include <thread>
#include <QObject>


class SocketServer : public QObject
{
Q_OBJECT


private:
    SOCKET ServerSocket;
    //SOCKET ClientSocket;
    SOCKADDR_IN ServerAddr;
    WSADATA wsdata;
    QSqlDatabase* db;
    QSqlQuery *query_socket;
    char buf_data_user[600];
    QString book_info;

public:
    SocketServer(QObject * parent = 0);
    virtual ~SocketServer();
    void InitializeSocket();
    void BindSocket();
    void ListhenSocket();
    void AcceptSocket();
    void ProcessingClient(SOCKET & ClientSocket);
    void AddNewUser(SOCKET  & ClientSocket, char * str);
    void CheckLog_user(SOCKET  & ClientSocket,char* log_str);
    void DBtoSock(QSqlDatabase* obj);
    void SearchAuthor(SOCKET  & ClientSocket);
    void SearchGenres(SOCKET  & ClientSocket);
    void ChoiseBooks(SOCKET  & ClientSocket);
    void SearchBook(SOCKET  & ClientSocket);
    void SearchBookGenres(SOCKET  & ClientSocket);
    void AddOrderToDB(SOCKET  & ClientSocket);


signals:

    void startClientThread(SOCKET * socket);
};

#endif // SOCKETSERVER_H
