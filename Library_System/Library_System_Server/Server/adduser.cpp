 #include "adduser.h"
#include "ui_adduser.h"
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QMessageBox>
adduser::adduser(QSqlDatabase* obj,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::adduser)
{
    ui->setupUi(this);
    db = obj;
}

adduser::~adduser()
{
    delete ui;
}

void adduser::on_SaveData_clicked()
{
    int count = 0;
    if(ui->lineName->text() == "" || ui->lineSurname->text() == "" || ui->lineEmail->text() == "" ||
            ui->lineLogin->text() == "" || ui->linePassword->text() == "")
    {
        QMessageBox::information(this,"Message", "All fields must be filled!");
    }
    else{
    QSqlQuery *query = new QSqlQuery(*db);
    query->exec("select* from users_info where Login = '" + ui->lineLogin->text() + "' or Email = '" + ui->lineEmail->text() + "'" );
    while(query->next())
    {
        count++;
    }
    if(count == 0){



       query->prepare("INSERT INTO users_info (idusers_info,Name,Surname,Email,Login,Password) "
              "VALUES (?,?, ?, ?, ?, ?)");
       query->addBindValue(NULL);
       query->addBindValue(ui->lineName->text());
       query->addBindValue(ui->lineSurname->text());
       query->addBindValue(ui->lineEmail->text());
       query->addBindValue(ui->lineLogin->text());
       query->addBindValue(ui->linePassword->text());
       query->exec();
       this->close();
    }
    else
        QMessageBox::information(this,"Message","The user of such data is already. Please try again");
    }


}
