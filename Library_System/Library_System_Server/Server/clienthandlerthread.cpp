#include "clienthandlerthread.h"

ClientHandlerThread::ClientHandlerThread(SocketServer * pSocket,SOCKET * ClientSocket){
    this->clientThread = pSocket;
    this->ClientSocket = ClientSocket;
}

void ClientHandlerThread::run(){

    this->clientThread->ProcessingClient(*this->ClientSocket);

}
