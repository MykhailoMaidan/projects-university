#ifndef CLIENTHANDLERTHREAD_H
#define CLIENTHANDLERTHREAD_H

#include <socketserver.h>
#include <winsock2.h>

#include <QThread>

class ClientHandlerThread : public QThread
{
public:
    ClientHandlerThread(SocketServer * pSocket,SOCKET * ClientSocket);
    void run();
private:
    SocketServer * clientThread;
    SOCKET * ClientSocket;


};

#endif // CLIENTHANDLERTHREAD_H
