/********************************************************************************
** Form generated from reading UI file 'registration.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGISTRATION_H
#define UI_REGISTRATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Registration
{
public:
    QGridLayout *gridLayout_2;
    QGroupBox *Registration_2;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *Yourname_layout;
    QLabel *label;
    QLineEdit *lineEdit;
    QHBoxLayout *Yousurname_layout;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QHBoxLayout *loginname_layout;
    QLabel *label_3;
    QLineEdit *lineEdit_3;
    QHBoxLayout *email_layout;
    QLabel *label_4;
    QLineEdit *lineEdit_4;
    QHBoxLayout *password_layout;
    QLabel *label_5;
    QLineEdit *lineEdit_5;
    QGridLayout *gridLayout_4;
    QPushButton *can_reg_button;
    QPushButton *Regis_button;

    void setupUi(QDialog *Registration)
    {
        if (Registration->objectName().isEmpty())
            Registration->setObjectName(QStringLiteral("Registration"));
        Registration->resize(1600, 309);
        QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Registration->sizePolicy().hasHeightForWidth());
        Registration->setSizePolicy(sizePolicy);
        Registration->setStyleSheet(QLatin1String("QPushButton{\n"
"                          background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(212, 146, 70, 255), stop:1 rgba(255, 255, 255, 255));\n"
"                          border-style: inset;\n"
"                          border-style: outset;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"                          padding: 6px;\n"
"}\n"
"QPushButton:pressed{\n"
"                          background-color: rgb(224, 200, 0);\n"
"                          border-style: inset;\n"
"                          color: red;\n"
"                          border-style: outset;\n"
"                          border-width: 3px;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"              "
                        "            padding: 6px;\n"
"}\n"
"QDialog\n"
"{\n"
" background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(50, 88, 99, 255), stop:1 rgba(255, 255, 255, 255));\n"
"\n"
"}\n"
" QLineEdit {\n"
"     border: 2px solid gray;\n"
"     border-radius: 10px;\n"
"     padding: 0 8px;\n"
"     background: rgb(85, 170, 127);\n"
" }\n"
"QGroupBox::title {\n"
"  subcontrol-position: top center; /* position at the top center */\n"
"}\n"
"QGroupBox{\n"
"font: 75 12pt \"MS Shell Dlg 2\";\n"
"}\n"
"QLabel\n"
"{\n"
"font: 75 12pt \"MS Shell Dlg 2\";\n"
"}\n"
""));
        gridLayout_2 = new QGridLayout(Registration);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        Registration_2 = new QGroupBox(Registration);
        Registration_2->setObjectName(QStringLiteral("Registration_2"));
        gridLayout = new QGridLayout(Registration_2);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        Yourname_layout = new QHBoxLayout();
        Yourname_layout->setObjectName(QStringLiteral("Yourname_layout"));
        label = new QLabel(Registration_2);
        label->setObjectName(QStringLiteral("label"));

        Yourname_layout->addWidget(label);

        lineEdit = new QLineEdit(Registration_2);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        Yourname_layout->addWidget(lineEdit);


        verticalLayout->addLayout(Yourname_layout);

        Yousurname_layout = new QHBoxLayout();
        Yousurname_layout->setObjectName(QStringLiteral("Yousurname_layout"));
        label_2 = new QLabel(Registration_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        Yousurname_layout->addWidget(label_2);

        lineEdit_2 = new QLineEdit(Registration_2);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        Yousurname_layout->addWidget(lineEdit_2);


        verticalLayout->addLayout(Yousurname_layout);

        loginname_layout = new QHBoxLayout();
        loginname_layout->setObjectName(QStringLiteral("loginname_layout"));
        label_3 = new QLabel(Registration_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        loginname_layout->addWidget(label_3);

        lineEdit_3 = new QLineEdit(Registration_2);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        loginname_layout->addWidget(lineEdit_3);


        verticalLayout->addLayout(loginname_layout);

        email_layout = new QHBoxLayout();
        email_layout->setObjectName(QStringLiteral("email_layout"));
        label_4 = new QLabel(Registration_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        email_layout->addWidget(label_4);

        lineEdit_4 = new QLineEdit(Registration_2);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));

        email_layout->addWidget(lineEdit_4);


        verticalLayout->addLayout(email_layout);

        password_layout = new QHBoxLayout();
        password_layout->setObjectName(QStringLiteral("password_layout"));
        label_5 = new QLabel(Registration_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        password_layout->addWidget(label_5);

        lineEdit_5 = new QLineEdit(Registration_2);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));

        password_layout->addWidget(lineEdit_5);


        verticalLayout->addLayout(password_layout);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        gridLayout_2->addWidget(Registration_2, 0, 0, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        can_reg_button = new QPushButton(Registration);
        can_reg_button->setObjectName(QStringLiteral("can_reg_button"));

        gridLayout_4->addWidget(can_reg_button, 2, 0, 1, 1);

        Regis_button = new QPushButton(Registration);
        Regis_button->setObjectName(QStringLiteral("Regis_button"));

        gridLayout_4->addWidget(Regis_button, 1, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout_4, 0, 1, 1, 1);


        retranslateUi(Registration);

        QMetaObject::connectSlotsByName(Registration);
    } // setupUi

    void retranslateUi(QDialog *Registration)
    {
        Registration->setWindowTitle(QApplication::translate("Registration", "Dialog", nullptr));
        Registration_2->setTitle(QApplication::translate("Registration", "Registration", nullptr));
        label->setText(QApplication::translate("Registration", "Your name", nullptr));
        label_2->setText(QApplication::translate("Registration", "Your surname", nullptr));
        label_3->setText(QApplication::translate("Registration", "Login name", nullptr));
        label_4->setText(QApplication::translate("Registration", "Email address", nullptr));
        label_5->setText(QApplication::translate("Registration", "Password", nullptr));
        can_reg_button->setText(QApplication::translate("Registration", "Cancel", nullptr));
        Regis_button->setText(QApplication::translate("Registration", "Registration", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Registration: public Ui_Registration {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGISTRATION_H
