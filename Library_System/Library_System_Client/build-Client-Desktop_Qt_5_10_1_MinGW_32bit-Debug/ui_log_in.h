/********************************************************************************
** Form generated from reading UI file 'log_in.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOG_IN_H
#define UI_LOG_IN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Log_in
{
public:
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QPushButton *Cancle_button;
    QPushButton *OK_button;

    void setupUi(QDialog *Log_in)
    {
        if (Log_in->objectName().isEmpty())
            Log_in->setObjectName(QStringLiteral("Log_in"));
        Log_in->resize(1600, 309);
        Log_in->setStyleSheet(QLatin1String("QPushButton{\n"
"                          background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(212, 146, 70, 255), stop:1 rgba(255, 255, 255, 255));\n"
"                          border-style: inset;\n"
"                          border-style: outset;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"                          padding: 6px;\n"
"}\n"
"QPushButton:pressed{\n"
"                          background-color: rgb(224, 200, 0);\n"
"                          border-style: inset;\n"
"                          color: red;\n"
"                          border-style: outset;\n"
"                          border-width: 3px;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"              "
                        "            padding: 6px;\n"
"}\n"
"QDialog\n"
"{\n"
" background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(50, 88, 99, 255), stop:1 rgba(255, 255, 255, 255))\n"
"}\n"
" QLineEdit {\n"
"     border: 2px solid gray;\n"
"     border-radius: 10px;\n"
"     padding: 0 8px;\n"
"     background: rgb(85, 170, 127);\n"
" }\n"
"QGroupBox::title {\n"
"  subcontrol-position: top center; /* position at the top center */\n"
"}\n"
"QGroupBox{\n"
"font: 75 12pt \"MS Shell Dlg 2\";\n"
"}\n"
"QLabel\n"
"{\n"
"font: 75 12pt \"MS Shell Dlg 2\";\n"
"}\n"
""));
        verticalLayout_3 = new QVBoxLayout(Log_in);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBox = new QGroupBox(Log_in);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_2->addWidget(label);

        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        lineEdit_2 = new QLineEdit(groupBox);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setEnabled(true);

        horizontalLayout->addWidget(lineEdit_2);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addLayout(verticalLayout);


        verticalLayout_3->addWidget(groupBox);

        Cancle_button = new QPushButton(Log_in);
        Cancle_button->setObjectName(QStringLiteral("Cancle_button"));

        verticalLayout_3->addWidget(Cancle_button);

        OK_button = new QPushButton(Log_in);
        OK_button->setObjectName(QStringLiteral("OK_button"));
        OK_button->setEnabled(true);

        verticalLayout_3->addWidget(OK_button);


        retranslateUi(Log_in);

        QMetaObject::connectSlotsByName(Log_in);
    } // setupUi

    void retranslateUi(QDialog *Log_in)
    {
        Log_in->setWindowTitle(QApplication::translate("Log_in", "Dialog", nullptr));
        groupBox->setTitle(QApplication::translate("Log_in", "Log in", nullptr));
        label->setText(QApplication::translate("Log_in", "Enter username", nullptr));
        label_2->setText(QApplication::translate("Log_in", "Enter password", nullptr));
        Cancle_button->setText(QApplication::translate("Log_in", "Cancel", nullptr));
        OK_button->setText(QApplication::translate("Log_in", "OK", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Log_in: public Ui_Log_in {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOG_IN_H
