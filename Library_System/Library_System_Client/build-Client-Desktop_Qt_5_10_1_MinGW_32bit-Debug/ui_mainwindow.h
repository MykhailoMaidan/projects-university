/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_3;
    QLabel *user_name;
    QSpacerItem *verticalSpacer;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QPushButton *Log_in_button;
    QPushButton *Menu_books_button;
    QPushButton *pushButton;
    QPushButton *Registratio_button;
    QLabel *label;
    QSpacerItem *verticalSpacer_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::ApplicationModal);
        MainWindow->resize(1000, 1000);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setStyleSheet(QLatin1String("QPushButton{\n"
"                          background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(212, 146, 70, 255), stop:1 rgba(255, 255, 255, 255));\n"
"                          border-style: inset;\n"
"                          border-style: outset;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"                          padding: 6px;\n"
"}\n"
"QPushButton:pressed{\n"
"                          background-color: rgb(224, 200, 0);\n"
"                          border-style: inset;\n"
"                          color: red;\n"
"                          border-style: outset;\n"
"                          border-width: 3px;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"              "
                        "            padding: 6px;\n"
"}\n"
"QMainWindow\n"
"{\n"
" background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(50, 88, 99, 255), stop:1 rgba(255, 255, 255, 255))\n"
"}"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_3 = new QGridLayout(centralWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        user_name = new QLabel(centralWidget);
        user_name->setObjectName(QStringLiteral("user_name"));

        gridLayout_3->addWidget(user_name, 0, 0, 3, 3);

        verticalSpacer = new QSpacerItem(20, 297, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer, 0, 3, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));

        gridLayout_3->addLayout(gridLayout_2, 1, 1, 3, 4);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setEnabled(true);
        groupBox->setStyleSheet(QStringLiteral(""));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        Log_in_button = new QPushButton(groupBox);
        Log_in_button->setObjectName(QStringLiteral("Log_in_button"));

        gridLayout->addWidget(Log_in_button, 0, 0, 1, 1);

        Menu_books_button = new QPushButton(groupBox);
        Menu_books_button->setObjectName(QStringLiteral("Menu_books_button"));

        gridLayout->addWidget(Menu_books_button, 2, 0, 1, 1);

        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setStyleSheet(QLatin1String("QPushButton{\n"
"                          background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(212, 146, 70, 255), stop:1 rgba(255, 255, 255, 255));\n"
"                          border-style: inset;\n"
"                          border-style: outset;\n"
"                          border-radius: 5px;\n"
"                          border-color: beige;\n"
"                          font: bold 4p;\n"
"                          min-width: 4em;\n"
"                          padding: 3px;\n"
"}"));

        gridLayout->addWidget(pushButton, 3, 0, 1, 1);

        Registratio_button = new QPushButton(groupBox);
        Registratio_button->setObjectName(QStringLiteral("Registratio_button"));

        gridLayout->addWidget(Registratio_button, 1, 0, 1, 1);


        gridLayout_3->addWidget(groupBox, 2, 2, 2, 2);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setEnabled(true);
        label->setPixmap(QPixmap(QString::fromUtf8(":/picture/book.jpg")));

        gridLayout_3->addWidget(label, 3, 4, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 296, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_2, 4, 4, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1000, 19));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        QWidget::setTabOrder(Log_in_button, Registratio_button);
        QWidget::setTabOrder(Registratio_button, Menu_books_button);
        QWidget::setTabOrder(Menu_books_button, pushButton);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        user_name->setText(QString());
        groupBox->setTitle(QString());
        Log_in_button->setText(QApplication::translate("MainWindow", "Log in", nullptr));
        Menu_books_button->setText(QApplication::translate("MainWindow", "Menu books", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "Exit", nullptr));
        Registratio_button->setText(QApplication::translate("MainWindow", "Registration", nullptr));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
