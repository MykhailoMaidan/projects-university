/********************************************************************************
** Form generated from reading UI file 'buy_books.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BUY_BOOKS_H
#define UI_BUY_BOOKS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Buy_Books
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *nameuser_label;
    QGridLayout *gridLayout;
    QLineEdit *PhoneNumber_lineEdit;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *Address_line;
    QTableWidget *Books_ordertable;
    QGridLayout *gridLayout_2;
    QPushButton *Buy_button;
    QPushButton *Delete_button;
    QPushButton *Cancel_order;

    void setupUi(QWidget *Buy_Books)
    {
        if (Buy_Books->objectName().isEmpty())
            Buy_Books->setObjectName(QStringLiteral("Buy_Books"));
        Buy_Books->setWindowModality(Qt::NonModal);
        Buy_Books->resize(619, 309);
        Buy_Books->setContextMenuPolicy(Qt::NoContextMenu);
        Buy_Books->setStyleSheet(QLatin1String("QPushButton{\n"
"                          background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(212, 146, 70, 255), stop:1 rgba(255, 255, 255, 255));\n"
"                          border-style: inset;\n"
"                          border-style: outset;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"                          padding: 6px;\n"
"}\n"
"QPushButton:pressed{\n"
"                          background-color: rgb(224, 200, 0);\n"
"                          border-style: inset;\n"
"                          color: red;\n"
"                          border-style: outset;\n"
"                          border-width: 3px;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"              "
                        "            padding: 6px;\n"
"}\n"
"QDialog\n"
"{\n"
" background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(50, 88, 99, 255), stop:1 rgba(255, 255, 255, 255))\n"
"}\n"
" QLineEdit {\n"
"     border: 2px solid gray;\n"
"     border-radius: 10px;\n"
"     padding: 0 8px;\n"
"     background: rgb(85, 170, 127);\n"
" }\n"
"QGroupBox::title {\n"
"  subcontrol-position: top center; /* position at the top center */\n"
"}\n"
"QGroupBox{\n"
"font: 75 12pt \"MS Shell Dlg 2\";\n"
"}\n"
"QLabel\n"
"{\n"
"font: 75 12pt \"MS Shell Dlg 2\";\n"
"}\n"
"QTableWidget {\n"
"    background-color: qlineargradient(spread:repeat, x1:0, y1:0, x2:1, y2:1, stop:0.272727 rgba(203, 228, 225, 255))\n"
"}\n"
"QHeaderView, QHeaderView::section {\n"
"    background-color: rgba(128, 128, 128, 128);\n"
"}"));
        verticalLayout = new QVBoxLayout(Buy_Books);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        nameuser_label = new QLabel(Buy_Books);
        nameuser_label->setObjectName(QStringLiteral("nameuser_label"));

        verticalLayout->addWidget(nameuser_label);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        PhoneNumber_lineEdit = new QLineEdit(Buy_Books);
        PhoneNumber_lineEdit->setObjectName(QStringLiteral("PhoneNumber_lineEdit"));

        gridLayout->addWidget(PhoneNumber_lineEdit, 2, 0, 1, 1);

        label = new QLabel(Buy_Books);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        label_2 = new QLabel(Buy_Books);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout->addWidget(label_2);

        Address_line = new QLineEdit(Buy_Books);
        Address_line->setObjectName(QStringLiteral("Address_line"));

        verticalLayout->addWidget(Address_line);

        Books_ordertable = new QTableWidget(Buy_Books);
        Books_ordertable->setObjectName(QStringLiteral("Books_ordertable"));
        Books_ordertable->horizontalHeader()->setDefaultSectionSize(130);

        verticalLayout->addWidget(Books_ordertable);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));

        verticalLayout->addLayout(gridLayout_2);

        Buy_button = new QPushButton(Buy_Books);
        Buy_button->setObjectName(QStringLiteral("Buy_button"));

        verticalLayout->addWidget(Buy_button);

        Delete_button = new QPushButton(Buy_Books);
        Delete_button->setObjectName(QStringLiteral("Delete_button"));

        verticalLayout->addWidget(Delete_button);

        Cancel_order = new QPushButton(Buy_Books);
        Cancel_order->setObjectName(QStringLiteral("Cancel_order"));

        verticalLayout->addWidget(Cancel_order);


        retranslateUi(Buy_Books);

        QMetaObject::connectSlotsByName(Buy_Books);
    } // setupUi

    void retranslateUi(QWidget *Buy_Books)
    {
        Buy_Books->setWindowTitle(QApplication::translate("Buy_Books", "Form", nullptr));
        nameuser_label->setText(QString());
        label->setText(QApplication::translate("Buy_Books", "Number telephone", nullptr));
        label_2->setText(QApplication::translate("Buy_Books", "\320\220ddress", nullptr));
        Buy_button->setText(QApplication::translate("Buy_Books", "BUY", nullptr));
        Delete_button->setText(QApplication::translate("Buy_Books", "Delete", nullptr));
        Cancel_order->setText(QApplication::translate("Buy_Books", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Buy_Books: public Ui_Buy_Books {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BUY_BOOKS_H
