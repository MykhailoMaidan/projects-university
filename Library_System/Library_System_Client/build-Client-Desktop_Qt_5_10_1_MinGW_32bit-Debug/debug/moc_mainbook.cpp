/****************************************************************************
** Meta object code from reading C++ file 'mainbook.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Client/mainbook.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainbook.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainBook_t {
    QByteArrayData data[15];
    char stringdata0[271];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainBook_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainBook_t qt_meta_stringdata_MainBook = {
    {
QT_MOC_LITERAL(0, 0, 8), // "MainBook"
QT_MOC_LITERAL(1, 9, 23), // "on_CancleButton_clicked"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 23), // "on_AuthorButton_clicked"
QT_MOC_LITERAL(4, 58, 13), // "ChoiseCatalog"
QT_MOC_LITERAL(5, 72, 17), // "QTableWidgetItem*"
QT_MOC_LITERAL(6, 90, 7), // "current"
QT_MOC_LITERAL(7, 98, 23), // "on_GenresButton_clicked"
QT_MOC_LITERAL(8, 122, 30), // "on_TableForCatalog_itemClicked"
QT_MOC_LITERAL(9, 153, 4), // "item"
QT_MOC_LITERAL(10, 158, 28), // "on_Search_pushButton_clicked"
QT_MOC_LITERAL(11, 187, 30), // "on_SearchLine_edit_textChanged"
QT_MOC_LITERAL(12, 218, 4), // "arg1"
QT_MOC_LITERAL(13, 223, 25), // "on_Books_info_itemClicked"
QT_MOC_LITERAL(14, 249, 21) // "on_Buy_button_clicked"

    },
    "MainBook\0on_CancleButton_clicked\0\0"
    "on_AuthorButton_clicked\0ChoiseCatalog\0"
    "QTableWidgetItem*\0current\0"
    "on_GenresButton_clicked\0"
    "on_TableForCatalog_itemClicked\0item\0"
    "on_Search_pushButton_clicked\0"
    "on_SearchLine_edit_textChanged\0arg1\0"
    "on_Books_info_itemClicked\0"
    "on_Buy_button_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainBook[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    1,   61,    2, 0x08 /* Private */,
       7,    0,   64,    2, 0x08 /* Private */,
       8,    1,   65,    2, 0x08 /* Private */,
      10,    0,   68,    2, 0x08 /* Private */,
      11,    1,   69,    2, 0x08 /* Private */,
      13,    1,   72,    2, 0x08 /* Private */,
      14,    0,   75,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, 0x80000000 | 5,    9,
    QMetaType::Void,

       0        // eod
};

void MainBook::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainBook *_t = static_cast<MainBook *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_CancleButton_clicked(); break;
        case 1: _t->on_AuthorButton_clicked(); break;
        case 2: _t->ChoiseCatalog((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 3: _t->on_GenresButton_clicked(); break;
        case 4: _t->on_TableForCatalog_itemClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 5: _t->on_Search_pushButton_clicked(); break;
        case 6: _t->on_SearchLine_edit_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_Books_info_itemClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 8: _t->on_Buy_button_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainBook::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_MainBook.data,
      qt_meta_data_MainBook,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainBook::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainBook::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainBook.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int MainBook::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
