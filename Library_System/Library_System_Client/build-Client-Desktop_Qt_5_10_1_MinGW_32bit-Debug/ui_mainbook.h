/********************************************************************************
** Form generated from reading UI file 'mainbook.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINBOOK_H
#define UI_MAINBOOK_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_MainBook
{
public:
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QLineEdit *SearchLine_edit;
    QCheckBox *Author_checkBox;
    QSplitter *splitter;
    QTableWidget *TableForCatalog;
    QTableWidget *Books_info;
    QHBoxLayout *horizontalLayout;
    QSpinBox *Counter_spinBox;
    QLabel *username_label;
    QPushButton *Buy_button;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QPushButton *AuthorButton;
    QPushButton *GenresButton;
    QPushButton *Search_pushButton;
    QPushButton *CancleButton;

    void setupUi(QDialog *MainBook)
    {
        if (MainBook->objectName().isEmpty())
            MainBook->setObjectName(QStringLiteral("MainBook"));
        MainBook->resize(1600, 429);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainBook->sizePolicy().hasHeightForWidth());
        MainBook->setSizePolicy(sizePolicy);
        MainBook->setStyleSheet(QLatin1String("QPushButton{\n"
"                          background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(212, 146, 70, 255), stop:1 rgba(255, 255, 255, 255));\n"
"                          border-style: inset;\n"
"                          border-style: outset;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"                          padding: 6px;\n"
"}\n"
"QPushButton:pressed{\n"
"                          background-color: rgb(224, 200, 0);\n"
"                          border-style: inset;\n"
"                          color: red;\n"
"                          border-style: outset;\n"
"                          border-width: 3px;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 10em;\n"
"              "
                        "            padding: 6px;\n"
"}\n"
"QDialog\n"
"{\n"
" background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(50, 88, 99, 255), stop:1 rgba(255, 255, 255, 255))\n"
"}\n"
"QTableWidget {\n"
"    background-color: qlineargradient(spread:repeat, x1:0, y1:0, x2:1, y2:1, stop:0.272727 rgba(203, 228, 225, 255))\n"
"}\n"
"QHeaderView, QHeaderView::section {\n"
"    background-color: rgba(128, 128, 128, 128);\n"
"}\n"
"QLineEdit {\n"
"     border: 2px solid gray;\n"
"     border-radius: 10px;\n"
"     padding: 0 8px;\n"
"     background: rgb(85, 170, 127);\n"
" }\n"
"QGroupBox::title {\n"
"  subcontrol-position: top center; /* position at the top center */\n"
"}\n"
"QGroupBox{\n"
"font: 75 12pt \"MS Shell Dlg 2\";\n"
"}\n"
"QLabel\n"
"{\n"
"font: 75 12pt \"MS Shell Dlg 2\";\n"
"}\n"
"QCheckBox {\n"
"   spacing: 5px;                          \n"
"	border-style: inset;\n"
"	border-style: outset;\n"
"     border-radius: 10px;\n"
"}\n"
"\n"
" QCheckBox::indicator {\n"
" width: 13px;\n"
" height: "
                        "13px;\n"
" }\n"
""));
        verticalLayout_3 = new QVBoxLayout(MainBook);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        SearchLine_edit = new QLineEdit(MainBook);
        SearchLine_edit->setObjectName(QStringLiteral("SearchLine_edit"));

        verticalLayout->addWidget(SearchLine_edit);

        Author_checkBox = new QCheckBox(MainBook);
        Author_checkBox->setObjectName(QStringLiteral("Author_checkBox"));

        verticalLayout->addWidget(Author_checkBox);


        verticalLayout_3->addLayout(verticalLayout);

        splitter = new QSplitter(MainBook);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        TableForCatalog = new QTableWidget(splitter);
        TableForCatalog->setObjectName(QStringLiteral("TableForCatalog"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(180);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(TableForCatalog->sizePolicy().hasHeightForWidth());
        TableForCatalog->setSizePolicy(sizePolicy1);
        TableForCatalog->setSizeIncrement(QSize(0, 0));
        TableForCatalog->setMidLineWidth(171);
        TableForCatalog->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        TableForCatalog->setRowCount(0);
        splitter->addWidget(TableForCatalog);
        TableForCatalog->horizontalHeader()->setDefaultSectionSize(201);
        Books_info = new QTableWidget(splitter);
        Books_info->setObjectName(QStringLiteral("Books_info"));
        splitter->addWidget(Books_info);
        Books_info->horizontalHeader()->setDefaultSectionSize(105);

        verticalLayout_3->addWidget(splitter);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        Counter_spinBox = new QSpinBox(MainBook);
        Counter_spinBox->setObjectName(QStringLiteral("Counter_spinBox"));

        horizontalLayout->addWidget(Counter_spinBox);

        username_label = new QLabel(MainBook);
        username_label->setObjectName(QStringLiteral("username_label"));

        horizontalLayout->addWidget(username_label);

        Buy_button = new QPushButton(MainBook);
        Buy_button->setObjectName(QStringLiteral("Buy_button"));
        Buy_button->setStyleSheet(QLatin1String("QPushButton{\n"
"                          background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(212, 146, 70, 255), stop:1 rgba(255, 255, 255, 255));\n"
"                          border-style: inset;\n"
"                          border-style: outset;\n"
"                          border-radius: 10px;\n"
"                          border-color: beige;\n"
"                          font: bold 14p;\n"
"                          min-width: 2em;\n"
"                          padding: 6px;\n"
"}"));

        horizontalLayout->addWidget(Buy_button);


        verticalLayout_3->addLayout(horizontalLayout);

        groupBox = new QGroupBox(MainBook);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        AuthorButton = new QPushButton(groupBox);
        AuthorButton->setObjectName(QStringLiteral("AuthorButton"));

        verticalLayout_2->addWidget(AuthorButton);

        GenresButton = new QPushButton(groupBox);
        GenresButton->setObjectName(QStringLiteral("GenresButton"));

        verticalLayout_2->addWidget(GenresButton);

        Search_pushButton = new QPushButton(groupBox);
        Search_pushButton->setObjectName(QStringLiteral("Search_pushButton"));

        verticalLayout_2->addWidget(Search_pushButton);

        CancleButton = new QPushButton(groupBox);
        CancleButton->setObjectName(QStringLiteral("CancleButton"));
        CancleButton->setStyleSheet(QStringLiteral(""));

        verticalLayout_2->addWidget(CancleButton);


        verticalLayout_3->addWidget(groupBox);


        retranslateUi(MainBook);

        QMetaObject::connectSlotsByName(MainBook);
    } // setupUi

    void retranslateUi(QDialog *MainBook)
    {
        MainBook->setWindowTitle(QApplication::translate("MainBook", "Dialog", nullptr));
        Author_checkBox->setText(QApplication::translate("MainBook", "Author", nullptr));
        username_label->setText(QApplication::translate("MainBook", "TextLabel", nullptr));
        Buy_button->setText(QApplication::translate("MainBook", "Buy", nullptr));
        groupBox->setTitle(QApplication::translate("MainBook", "Menu Books", nullptr));
        AuthorButton->setText(QApplication::translate("MainBook", "Author", nullptr));
        GenresButton->setText(QApplication::translate("MainBook", "Genres", nullptr));
        Search_pushButton->setText(QApplication::translate("MainBook", "Search", nullptr));
        CancleButton->setText(QApplication::translate("MainBook", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainBook: public Ui_MainBook {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINBOOK_H
