#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    this->setFixedSize(1036,590);
    frequency_symbol.resize(34);
    mass_frequency.resize(34);
    mass_one.resize(34);
    for(int i = 0; i < 34; i++)
    {
        mass_one[i] = i;
    }
    map_symbol = new QMap<QChar,int>;
    ui->setupUi(this);
    map_symbol->insert(' ',0);
    map_symbol->insert(L'а',1);
    map_symbol->insert(L'б',2);
    map_symbol->insert(L'в',3);
    map_symbol->insert(L'г',4);
    map_symbol->insert(L'ґ',5);
    map_symbol->insert(L'д',6);
    map_symbol->insert(L'е',7);
    map_symbol->insert(L'є',8);
    map_symbol->insert(L'ж',9);
    map_symbol->insert(L'з',10);
    map_symbol->insert(L'и',11);
    map_symbol->insert(L'і',12);
    map_symbol->insert(L'ї',13);
    map_symbol->insert(L'й',14);
    map_symbol->insert(L'к',15);
    map_symbol->insert(L'л',16);
    map_symbol->insert(L'м',17);
    map_symbol->insert(L'н',18);
    map_symbol->insert(L'о',19);
    map_symbol->insert(L'п',20);
    map_symbol->insert(L'р',21);
    map_symbol->insert(L'с',22);
    map_symbol->insert(L'т',23);
    map_symbol->insert(L'у',24);
    map_symbol->insert(L'ф',25);
    map_symbol->insert(L'х',26);
    map_symbol->insert(L'ц',27);
    map_symbol->insert(L'ч',28);
    map_symbol->insert(L'ш',29);
    map_symbol->insert(L'щ',30);
    map_symbol->insert(L'ь',31);
    map_symbol->insert(L'ю',32);
    map_symbol->insert(L'я',33);


    SetGraph();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_Size_spinBox_valueChanged(int arg1)
{
    ui->Key_tableWidget->setColumnCount(arg1);
    ui->Key_tableWidget->setRowCount(arg1);
}

void MainWindow::on_Set_key_pushButton_clicked()
{
    if(ui->Key_lineEdit->text().isEmpty())
    {
        QMessageBox::information(this,"Information","No key!");
        return;
    }
    if(!FindKey())
    {
        QMessageBox::information(this,"Information","The key is wrong!");
    }
    else
    {

        det = Det(mass_key,ui->Key_tableWidget->rowCount());

    if(det == 0)
    {
        QMessageBox::information(this,"Information","Determinant is zero!");
    }
    else
    {
        if(ReverseElement(std::abs(det),34,inverse_element) == 1)
        {
            QMessageBox::information(this,"Information","Inverse element exists!");
        }
        else
        {
            if(inverse_element < 0)
            {
                inverse_element = (inverse_element + 34) % 34;

            }
        }
    }
 }
}

bool MainWindow::FindKey()
{
    QString temp = ui->Key_lineEdit->text();
    QMap <QChar,int>::iterator it;

    if(ui->Key_lineEdit->text().length() != ui->Key_tableWidget->rowCount()*ui->Key_tableWidget->columnCount())
    {

        return 0 ;
    }
    else
    {
        {
            int k = 0;
            mass_key = SelectFreeMemory(mass_key, ui->Key_tableWidget->rowCount(),ui->Key_tableWidget->columnCount());
           QTableWidgetItem* item =  new QTableWidgetItem[ui->Key_tableWidget->rowCount()*ui->Key_tableWidget->columnCount()];
            for(int i = 0; i < ui->Key_tableWidget->rowCount();i++)
            {
                int n = 0;
                for(int j = 0; j < ui->Key_tableWidget->columnCount(); j++)
                {

                    it = map_symbol->find(temp[k].toLower());
                    item[k].setText(QString::number(it.value()));
                    ui->Key_tableWidget->setItem(i,j,&item[k]);
                    mass_key[i][j] = it.value();
                    n++;
                    k++;
                }

            }
        }
        return 1;
    }

}

int** MainWindow::SelectFreeMemory(int** mass, int n, int m)
{
    mass = new int*[n];

    for(int i = 0; i < n; i++)
    {
        mass[i] = new int[m];
    }
    return mass;
}

void MainWindow::Get_matr(int **matr, int n, int **temp_matr, int indRow, int indCol)
{
    int ki = 0;
        for (int i = 0; i < n; i++){
            if(i != indRow){
                for (int j = 0, kj = 0; j < n; j++){
                    if (j != indCol){
                        temp_matr[ki][kj] = matr[i][j];
                        kj++;
                    }
                }
                ki++;
            }
        }
}

int MainWindow::Det(int**matr, int n)
{
    int temp = 0;
    int k = 1;
    if(n < 1){

        return 0;
    }
    else if (n == 1)
        temp = matr[0][0];
    else if (n == 2)
        temp = matr[0][0] * matr[1][1] - matr[1][0] * matr[0][1];
    else{
        for(int i = 0; i < n; i++){
            int m = n - 1;
            int **temp_matr = new int * [m];
            for(int j = 0; j < m; j++)
                temp_matr[j] = new int [m];
            Get_matr(matr, n, temp_matr, 0, i);
            temp = temp + k * matr[0][i] * Det(temp_matr, m);
            k = -k;

        }
    }
    return temp;
}

int MainWindow::gcdex(int a, int b, int &x, int &y)
{
    if (b == 0) {
        x = 1;
        y = 0;
        return a;
    }
    int x1, y1;
    int d1 = gcdex(b, a % b, x1, y1);
    x = y1;
    y = x1 - (a / b) * y1;
    return d1;
}

int MainWindow::ReverseElement(int a, int N, int &result)
{
    int x, y, d;
    d = gcdex(a, N, x, y);
    if (d != 1)
    {
        return 1;
    }
    else
    {
        result = x;
        return 0;
    }
}

void MainWindow::Frequency(QString  temp_symbol)
{
    QMap<QChar,int>::iterator it;
    int count = 0;
    for(int i = 0; i < 34; i++)
    {
        count = 0;
        for(int j = 0; j < temp_symbol.length(); j++)
        {
            if(map_symbol->key(i) == temp_symbol[j].toLower())
            {
                count++;
            }

        }
        map_frequency.insert(map_symbol->key(i),(double)count/(double)ui->Set_text_lineEdit->text().length());
        frequency_symbol[i] = (double)count/(double)ui->Set_text_lineEdit->text().length();
        mass_frequency[i] = (double)count/(double)ui->Set_text_lineEdit->text().length();
    }
}

void MainWindow::on_Set_Text_pushButton_clicked()
{

    text = ui->Set_text_lineEdit->text();
    int n = 0;
    QMap<QChar,int>::iterator it;

    if(!ui->Set_text_lineEdit->text().length())
    {
        QMessageBox::information(this,"Information","Enter text!");
    }
    else
    {
        for(int i = 0; i < text.length(); i++)
        {

            it = map_symbol->find(text[i].toLower());
            n = (2 * it.value()) % ui->Size_spinBox->value();
            if(n  == 0)
            {
                continue;
            }
            else
            {
            for(int j = i; j <i+n; j++)
            {
                map_frequency.clear();
                Frequency(text);
                double min = frequency_symbol.min();
                text.insert(j+1,map_frequency.key(min));

            }
             i = i+n;
            }
        }
        FindTextNumber(text);
    }
}

void MainWindow::on_Graph_pushButton_clicked()
{
    if(!ui->Set_text_lineEdit->text().length())
    {
        QMessageBox::information(this,"Information","Enter text!");
    }
    else
    {
    Frequency(text);

    ui->Grafic_widget->graph(0)->setData(mass_one,mass_frequency);
    ui->Grafic_widget->replot();
    }
}

void MainWindow::SetGraph()
{
    ui->Grafic_widget->clearGraphs();
    ui->Grafic_widget->addGraph(ui->Grafic_widget->xAxis,ui->Grafic_widget->yAxis);
    QVector<double> ticks;
    QVector<QString> labels;

    for(int  i = 0; i < 34; i++)
    {
        ticks << i;
    }

    labels << "Sp" << "а" << "б" << "в" << "г" << "ґ" << "д" << "е" << "є" << "ж" << "з" << "и" << "і" << "ї"
           << "й" << "к" << "л" << "м" << "н" << "о" << "п" << "р" << "с" << "т" << "у" << "ф" << "x" << "ц"
           << "ч" << "ш" << "щ" << "ь" << "ю" << "я";
    ui->Grafic_widget->xAxis->setAutoTicks(false);
    ui->Grafic_widget->xAxis->setAutoTickLabels(false);
    ui->Grafic_widget->xAxis->setTickVector(ticks);
    ui->Grafic_widget->xAxis->setTickVectorLabels(labels);
    ui->Grafic_widget->xAxis->setRange(0, 34);
    ui->Grafic_widget->yAxis->setRange(0, 1);
}

void MainWindow::FindTextNumber(QString text)
{
    QTableWidgetItem * item_text;
    ui->Text_tableWidget_2->setRowCount(ui->Size_spinBox->value());
    if(text.length()%ui->Size_spinBox->value() != 0)
    {
        ui->Text_tableWidget_2->setColumnCount((text.length()/ui->Size_spinBox->value()) + 1);
        item_text = new QTableWidgetItem[((text.length()/ui->Size_spinBox->value()) + 1)*
                ui->Size_spinBox->value()];
    }
    else
    {
        ui->Text_tableWidget_2->setColumnCount((text.length()/ui->Size_spinBox->value()));
        item_text = new QTableWidgetItem[((text.length()/ui->Size_spinBox->value()))*
                ui->Size_spinBox->value()];
    }

    mass_text = SelectFreeMemory(mass_text,ui->Text_tableWidget_2->rowCount(),ui->Text_tableWidget_2->columnCount());

    QMap<QChar,int>::iterator it;
    int count = 0;
    for(int i = 0; i < ui->Text_tableWidget_2->columnCount(); i++)
    {
        for(int j = 0; j < ui->Text_tableWidget_2->rowCount(); j++)
        if(count >= text.length())
        {
            item_text[count].setText(QString::number(33));
            ui->Text_tableWidget_2->setItem(j,i,&item_text[count]);
            count++;
            mass_text[j][i] = 33;
        }
        else
        {
            it = map_symbol->find(text[count].toLower());
            item_text[count].setText(QString::number(it.value()));
            ui->Text_tableWidget_2->setItem(j,i,&item_text[count]);
            count++;
            mass_text[j][i] = it.value();
        }

    }
}

void MainWindow::on_Encrypted_pushButton_clicked()
{
    if(ui->Key_lineEdit->text().isEmpty() || ui->Set_text_lineEdit->text().isEmpty())
    {
        QMessageBox::information(this,"Information","No key or text");
    }
    else
    {
    mass_ress = SelectFreeMemory(mass_ress,ui->Key_tableWidget->rowCount(),ui->Text_tableWidget_2->columnCount());

    for(int i = 0; i < ui->Key_tableWidget->rowCount(); i++)
        {
            for(int j = 0; j < ui->Text_tableWidget_2->columnCount(); j++)
            {
                mass_ress[i][j] = 0;
            }
        }

    for (int i = 0; i < ui->Key_tableWidget->rowCount(); i++)
    {
        for (int j = 0; j < ui->Text_tableWidget_2->columnCount(); j++)
          {

            for (int k = 0; k < ui->Text_tableWidget_2->rowCount(); k++)
            {

                mass_ress[i][j] +=  mass_key[i][k] * mass_text[k][j];
            }

        }
    }

    for(int i = 0; i < ui->Key_tableWidget->rowCount(); i++)
    {
        for(int j = 0; j < ui->Text_tableWidget_2->columnCount(); j++)
        {
            mass_ress[i][j] = mass_ress[i][j] % 34;
        }
    }

    QString ecrypted_text;

   for(int i = 0; i < ui->Text_tableWidget_2->columnCount(); i++)
   {
       for(int j = 0; j < ui->Text_tableWidget_2->rowCount(); j++)
       {
           ecrypted_text += map_symbol->key(mass_ress[j][i]);
       }
   }

  ui->Encrypted_text_lineEdit->setText(ecrypted_text);
    }
}

void MainWindow::InversMatrix()
{
    obr_matr = SelectFreeMemory(obr_matr,ui->Key_tableWidget->rowCount(), ui->Key_tableWidget->columnCount());
    if(inverse_element){
            for(int i = 0; i < ui->Key_tableWidget->rowCount(); i++){
                for(int j = 0; j < ui->Key_tableWidget->columnCount(); j++){
                    int m = ui->Key_tableWidget->rowCount() - 1;
                    int **temp_matr = new int * [m];
                    for(int k = 0; k < m; k++)
                        temp_matr[k] = new int[m];
                    Get_matr(mass_key, ui->Key_tableWidget->rowCount(), temp_matr, i, j);
                    obr_matr[i][j] = pow(-1.0, i + j + 2) * Det(temp_matr, m);
                    FreeMem(temp_matr, m);
                }
            }
}
    transpo_matrix = SelectFreeMemory(transpo_matrix,ui->Key_tableWidget->rowCount(),ui->Key_tableWidget->columnCount());
    TransponMtx(obr_matr,transpo_matrix,ui->Key_tableWidget->rowCount());

}

void MainWindow::FreeMem(int **matr, int n)
{
    for(int i = 0; i < n; i++)
        delete [] matr[i];
    delete [] matr;
}

void MainWindow::on_Decrypted_pushButton_clicked()
{
    if(ui->Key_lineEdit->text().isEmpty() || ui->Set_text_lineEdit->text().isEmpty())
    {
        QMessageBox::information(this,"Information","No key or text");
    }
    else
    {
    InversMatrix();

    for(int i = 0; i < ui->Key_tableWidget->rowCount(); i++)
    {
        for(int j = 0; j < ui->Key_tableWidget->columnCount(); j++)
        {
            if(transpo_matrix[i][j] < 0)
            {
                transpo_matrix[i][j] = std::abs(inverse_element * transpo_matrix[i][j]) % 34;
            }
            else
            {
                transpo_matrix[i][j] = std::abs(transpo_matrix[i][j]*(34 - inverse_element)) % 34;
            }
        }
    }

    mass_decrypterd = SelectFreeMemory(mass_decrypterd,ui->Key_tableWidget->rowCount(),ui->Text_tableWidget_2->columnCount());

    for(int i = 0; i < ui->Key_tableWidget->rowCount(); i++)
    {
        for(int j = 0; j < ui->Text_tableWidget_2->columnCount(); j++)
        {
            mass_decrypterd[i][j] = 0;
        }
    }
    for (int i = 0; i < ui->Key_tableWidget->rowCount(); i++)
    {
        for (int j = 0; j < ui->Text_tableWidget_2->columnCount(); j++)
        {

            for (int k = 0; k < ui->Text_tableWidget_2->rowCount(); k++)
            {

                mass_decrypterd[i][j] +=  transpo_matrix[i][k] * mass_ress[k][j];
            }
            mass_decrypterd[i][j] = mass_decrypterd[i][j] % 34;
        }

    }





        QString decrypted_text;
    for(int i = 0; i < ui->Text_tableWidget_2->columnCount(); i++)
    {
        for(int j = 0; j < ui->Text_tableWidget_2->rowCount(); j++)
        {
            decrypted_text += map_symbol->key(mass_decrypterd[j][i]);
        }
    }
    QMap <QChar,int>::iterator it;
    int n = 0;
    for(int i = 0; i < decrypted_text.length(); i++)
    {

        it = map_symbol->find(decrypted_text[i].toLower());
        n = (2 * it.value()) % ui->Size_spinBox->value();
        if(n  == 0)
        {
            continue;
        }
        else
        {
           decrypted_text.remove(i+1,n);
        }
    }

    ui->decrypted_text_lineEdit->setText(decrypted_text);
    }
}


void MainWindow::TransponMtx(int **matr, int **tMatr, int n)
{
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            tMatr[j][i] = matr[i][j];
}

