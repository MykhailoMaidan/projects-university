#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QDataStream>
#include <QFile>
#include <QChar>
#include <QString>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QMessageBox>
#include <valarray>
#include <iostream>
#include <QVector>
#include <string>

 using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int** SelectFreeMemory(int** mass,int, int);

    void Get_matr(int **, int , int **, int , int );

    int Det(int**matr, int n);

    bool FindKey();

    int gcdex(int a, int b, int &x, int &y);

    int ReverseElement(int a, int N, int &result);

    void Frequency(QString temp_symbol);

    void FindTextNumber(QString);

    void TransponMtx(int **matr, int **tMatr, int n);

private slots:
    void on_Size_spinBox_valueChanged(int arg1);

    void on_Set_key_pushButton_clicked();

    void on_Set_Text_pushButton_clicked();

    void on_Graph_pushButton_clicked();

    void SetGraph();

    void on_Encrypted_pushButton_clicked();

    void InversMatrix();

    void FreeMem(int **matr, int n);

    void on_Decrypted_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QMap <QChar,int>* map_symbol;
    int** mass_key;
    int** mass_text;
    int** mass_ress;
    int** obr_matr;
    int** transpo_matrix;
    int** mass_decrypterd;
    int det;
    QVector <double> mass_frequency;
    QVector <double> mass_one;
    int inverse_element;
    valarray <double> frequency_symbol;
    QMap<QChar,double> map_frequency;
    QString text;
};

#endif // MAINWINDOW_H
